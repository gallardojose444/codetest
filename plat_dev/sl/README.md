SL(1): Cure your bad habit of mistyping
=======================================

SL (Steam Locomotive) runs across your terminal when you type "sl" as
you meant to type "ls". It's just a joke command, and not useful at
all.

![](demo.gif)

Options Available:  
sl              : displays a random train  
sl -c           : displays a C51 train  
sl -l           : displays a little train  
sl -a           : displays a D51 train with an accident  
sl -f           : displays a D51 train flying  
sl -c -a -f -l  : displays a little train flying with an accident  
sl -cafl        : behaves like former  
sl -n N         : N = 3 displays the 3rd train animation  

Incorrect input is ignored for all cases other than N

Combination of options behave as follows:  
    Train animation (C51, D51, little) selected with LIFO precedence.  
    Accident and flying animations are combined.  
    If -n N option is selected then all other options are ignored.  
        Incorrect input for N prompts user to enter correct input.  

Nth animation follows pattern:  
N = 1 ==> little train  
N = 2 ==> little train with an accident  
N = 3 ==> little train flying  
N = 4 ==> little train flying with an accident  
N = 5 ==> C51 train  
...  

Copyright 1993,1998,2014 Toyoda Masashi (mtoyoda@acm.org)
